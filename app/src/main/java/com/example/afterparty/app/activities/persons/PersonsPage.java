package com.example.afterparty.app.activities.persons;

import android.app.*;
import android.content.Intent;
import android.os.*;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.example.afterparty.app.Globals;
import com.example.afterparty.app.R;
import com.example.afterparty.app.activities.profile.ProfilePage;
import com.example.afterparty.app.service.SocketService;
import com.example.afterparty.app.util.animation.ExpandCollapseAnimation;
import com.example.afterparty.app.util.scroll.EndlessScrollListener;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PersonsPage extends Activity {

    private static final String TAG = "PersonsPage";

    private ListView listView;

    private PersonsAdapter adapter;
    private ArrayList<JSONObject> listItems;

    private final static Integer pageLimit = 30;
    private String lastSearch;

    private Socket mSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_people);

        listView = (ListView)findViewById(R.id.personsContainer);

        // Create arrayList
        listItems = new ArrayList<>();

        // Create adapter
        adapter = new PersonsAdapter(this, listItems);

        // Assign adapter to ListView
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(onClickPerson);

        listView.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                getUsers(null, page);
            }
        });

        mSocket = SocketService.getSocket(getApplicationContext());
        mSocket.on("user/list", onReceivedUsers);

        bindSearchInput();

        getUsers(null, 0);
    }

    private Emitter.Listener onReceivedUsers = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            insertUsers((JSONArray) args[0]);
        }
    };

    private final AdapterView.OnItemClickListener onClickPerson = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent intent = new Intent(getApplicationContext(), ProfilePage.class);
            intent.putExtra("USER_ID", listItems.get(position).optString("_id"));
            intent.putExtra("OWN_PROFILE", false);
            startActivity(intent);
        }
    };

    private void bindSearchInput() {
        final EditText mSearchInput = (EditText)findViewById(R.id.search);

        mSearchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void afterTextChanged(Editable s) {
                String searchValue = mSearchInput.getText().toString().trim();

                clearUsers();
                getUsers(searchValue, 0);
            }
        });
    }

    private void clearUsers() {
        listItems.clear();
        adapter.notifyDataSetChanged();
    }

    private void getUsers(String searchValue, Integer page) {
        // remove 'no-users-found' text
        findViewById(R.id.noUsersFound).setVisibility(View.GONE);

        // Show loading spinner
        findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);

        if (searchValue == null) {
            searchValue = lastSearch;
        } else {
            lastSearch = searchValue;
        }

        try {
            JSONObject params = new JSONObject();

            params.put("search", searchValue);
            params.put("token", Globals.getLocalToken(this));
            params.put("offset", page * pageLimit);
            params.put("limit", pageLimit);

            mSocket.emit("user/list", params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void insertUsers(final JSONArray users) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Hide loading spinner
                findViewById(R.id.loadingPanel).setVisibility(View.GONE);

                if (users.length() == 0) {
                    findViewById(R.id.noUsersFound).setVisibility(View.VISIBLE);
                } else {
                    for (int i = 0; i < users.length(); i++) {
                        listItems.add(users.optJSONObject(i));
                    }

                    adapter.notifyDataSetChanged();
                }
            }
        });
    }

    private View getViewByPosition(int pos, ListView listView) {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition ) {
            return listView.getAdapter().getView(pos, null, listView);
        } else {
            final int childIndex = pos - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }

    private void animateUsers() {
        View view = this.getViewByPosition(0, listView);

        // first we measure height, we need to do this because we used wrap_content
        // if we use a fixed height we could just pass that in px.
        ExpandCollapseAnimation.setHeightForWrapContent(this, view);
        ExpandCollapseAnimation expandAni = new ExpandCollapseAnimation(view, 1000);
        listView.getChildAt(0).startAnimation(expandAni);
    }
}
