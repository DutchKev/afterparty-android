package com.example.afterparty.app.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.afterparty.app.R;

public class FragmentSlideMenu extends Fragment {

    private boolean menuClosed = true;
    private View rootView;
    private View overlay;
    private View slideMenu;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        rootView = inflater.inflate(R.layout.fragment_slide_menu, container, false);

        overlay = rootView.findViewById(R.id.overlay);
        slideMenu = rootView.findViewById(R.id.slide_container);

        return rootView;
    }

    public void toggle() {
        menuClosed = !menuClosed;

        if (menuClosed) {
            overlay.animate().alpha(0.0f);
            slideMenu.animate().translationX(slideMenu.getWidth());

        } else {
            overlay.setVisibility(View.VISIBLE);
            overlay.animate().alpha(0.9f);
            slideMenu.animate().translationX(0);
        }
    }

    public final boolean isOpen() {
        return !menuClosed;
    }
}
