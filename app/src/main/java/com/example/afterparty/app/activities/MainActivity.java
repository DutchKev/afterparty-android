package com.example.afterparty.app.activities;

import android.app.*;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.*;
import com.example.afterparty.app.Globals;
import com.example.afterparty.app.service.SocketService;
import com.example.afterparty.app.util.auth.Auth;
import com.splunk.mint.Mint;

public class MainActivity extends Activity {

    private static final String TAG = "MainActivity";

    private Boolean checkedVersion  = false;
    private Boolean checkedToken    = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(null);

        Auth.checkToken(this, onAuthSuccess, onAuthFail);
    }

    private Runnable onAuthSuccess = new Runnable() {
        @Override
        public void run() {
            // Open socket.io
            startService(new Intent(getApplicationContext(), SocketService.class));

            // Forward to HomePage
            startActivity(new Intent(MainActivity.this, HomePage.class));
            finish();
        }
    };

    private Runnable onAuthFail = new Runnable() {
        @Override
        public void run() {
            // Close socket.io
            stopService(new Intent(getApplicationContext(), SocketService.class));

            // Forward to LoginPage
            startActivity(new Intent(MainActivity.this, LoginPage.class));
            finish();
        }
    };

    private Runnable onVersionSuccess = new Runnable() {
        @Override
        public void run() {
            String token = Globals.getLocalToken(MainActivity.this);

            // Check if token exists
            if (token == null || token.equals("")) {
                onAuthFail.run();
            } else {
                //checkToken(token);
            }
        }
    };

    private Runnable onVersionFail = new Runnable() {
        @Override
        public void run() {
            // TODO: Show update dialog
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which)
                    {
                        case DialogInterface.BUTTON_POSITIVE:
                            //Yes button clicked
                            //SelfInstall01Activity.this.finish(); Close The App.

                            //DownloadOnSDcard();
                            //InstallApplication();
                            //UnInstallApplication(PackageName.toString());

                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked

                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage("New Apk Available..").setPositiveButton("Yes Proceed", dialogClickListener)
                    .setNegativeButton("No.", dialogClickListener).show();
        }
    };
}