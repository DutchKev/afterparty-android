package com.example.afterparty.app.service;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class UserNotifications {

    private static final UserNotifications instance = new UserNotifications();

    private List<Integer> mUnreadMessages = new ArrayList<Integer>();

    public static UserNotifications getInstance() {
        return instance;
    }

    private UserNotifications() {}

    public List getUnreadMessages() {
        return mUnreadMessages;
    }

    public void setUnreadMessages(JSONArray messages) {

    }

    public void clearUnreadMessages(String conversation_id) {

    }
}
