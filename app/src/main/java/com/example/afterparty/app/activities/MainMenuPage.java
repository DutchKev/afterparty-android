package com.example.afterparty.app.activities;

import android.app.*;
import android.content.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import com.example.afterparty.app.Globals;
import com.example.afterparty.app.R;

public class MainMenuPage extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_main_menu);


		Button startChat = (Button)findViewById(R.id.btnToChat);
        startChat.setOnClickListener(onGoClickListener);
	}

	private final View.OnClickListener onGoClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View view) {
			// Open chat page
			Intent intent = new Intent(getApplicationContext(), ChatPage.class);
			startActivity(intent);
            finish();
		}
	};
}
