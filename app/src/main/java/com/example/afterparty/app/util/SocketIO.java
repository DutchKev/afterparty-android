package com.example.afterparty.app.util;
import android.content.Context;
import android.util.Log;

import android.widget.Toast;
import com.example.afterparty.app.Constants;
import com.example.afterparty.app.Globals;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.github.nkzawa.socketio.client.SocketIOException;

import java.net.URISyntaxException;

public class SocketIO {

    private static String TAG = "SocketIO";

    public static Socket socket;
    public static int count = 0;
    public static Socket getInstance(Context context) {
        if (socket == null) {
            try {
                String url  = Constants.getNodeUrl() + "?token=" + Globals.getLocalToken(context);
                //Toast.makeText(context, (++count), Toast.LENGTH_SHORT).show();
                IO.Options options = new IO.Options();
                //options.forceNew = true;
                //options.reconnection = true;

                Log.i(TAG, url);

                socket = IO.socket(url, options);

                socket
                        .on("event", socketOnEvent)
                        .on(Socket.EVENT_DISCONNECT, socketOnDisconnect)
                        .on(Socket.EVENT_CONNECT_ERROR, socketOnConnectError)
                        .on(Socket.EVENT_ERROR, socketOnEventError);

            } catch (URISyntaxException e) {
                Log.d("IO", e.getMessage());
            }
        } else {
            //Toast.makeText(context, "Has Socket" + (++count), Toast.LENGTH_SHORT).show();
            //Toast.makeText(context, "")
        }

        return socket;
    }

    private static Emitter.Listener socketOnEvent = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            for (Object o : args) {
                Log.d(TAG, "object: " + o.toString());
                if (o instanceof SocketIOException)
                    ((SocketIOException) o).printStackTrace();
            }
        }
    };

    private static Emitter.Listener socketOnDisconnect = new Emitter.Listener() {

        @Override
        public void call(Object... args) {
            Log.d("Error", "ERROR");
        }
    };

    private static Emitter.Listener socketOnConnectError = new Emitter.Listener() {

        @Override
        public void call(Object... args) {
            Log.d("Error", "eventConnectError");
            for (Object o : args) {
                Log.d(TAG, "object: " + o.toString());
                if (o instanceof SocketIOException)
                    ((SocketIOException) o).printStackTrace();
            }
        }
    };

    private static Emitter.Listener socketOnEventError = new Emitter.Listener() {

        @Override
        public void call(Object... args) {
            Log.d(TAG, "eventError");
        }
    };

    public static void destroyConnection() {
        socket
                .off("event", socketOnEvent)
                .off(Socket.EVENT_DISCONNECT, socketOnDisconnect)
                .off(Socket.EVENT_CONNECT_ERROR, socketOnConnectError)
                .off(Socket.EVENT_ERROR, socketOnEventError);

        socket.close();
    }
}
