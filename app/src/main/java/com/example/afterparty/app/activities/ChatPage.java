package com.example.afterparty.app.activities;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;
import com.example.afterparty.app.R;
import com.example.afterparty.app.fragments.chat.FragmentChat;
import com.example.afterparty.app.fragments.FragmentSlideMenu;
import com.example.afterparty.app.fragments.FragmentVideoHud;
import com.example.afterparty.app.util.SocketIO;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.splunk.mint.Mint;
import org.json.JSONObject;

public class ChatPage extends FragmentActivity {

    private static final String TAG = "ChatPage";

    private FragmentChat mFragmentChat;
    private FragmentSlideMenu mFragmentSlideMenu;
    private FragmentVideoHud mFragmentVideoHud;

    private JSONObject currentUser;

    private boolean chatOpen = true;

    private Socket mSocket = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_chat);

        Mint.initAndStartSession(ChatPage.this, "c33059e3");

        mFragmentSlideMenu = (FragmentSlideMenu) getSupportFragmentManager().findFragmentById(R.id.fragment_slide_menu);
        mFragmentVideoHud = (FragmentVideoHud) getSupportFragmentManager().findFragmentById(R.id.fragment_video_hud);
        mFragmentChat = (FragmentChat) getSupportFragmentManager().findFragmentById(R.id.fragment_chat_container);

        mSocket = SocketIO.getInstance(this);
        mSocket.on(Socket.EVENT_CONNECT, this.socketOnConnect);
        mSocket.on("chat/users/next", onNextUserData);
        mSocket.connect();
    }

    private void requestNextUser() {
        mSocket.emit("chat/users/next");
    }

    public Socket getSocket() {
        return mSocket;
    }

    private static int count = 0;
    private Emitter.Listener onNextUserData = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject result = ((JSONObject) args[0]);
                    //String error = result.optString("error");

                    //if (error == null) {
                        currentUser = result;
                        mFragmentVideoHud.updateHeaderName(currentUser.optString("username"), currentUser.optString("gender"));
                        mFragmentVideoHud.setVideoPlayback(currentUser.optString("video_url"));
                    //} else {
                        //Toast.makeText(getApplicationContext(), error + (++count), Toast.LENGTH_SHORT).show();
                    //}
                }
            });
        }
    };

    public void onHudButtonClick(int event) {
        switch (event) {
            case FragmentVideoHud.CLICKED_SLIDE_MENU:
                mFragmentSlideMenu.toggle();
                break;
            case FragmentVideoHud.CLICKED_NEXT_CHAT:
                requestNextUser();
                break;
            case FragmentVideoHud.CLICKED_TOGGLE_CHAT:
                togglePageLayout();
                break;
            case FragmentVideoHud.CLICKED_TOGGLE_ORIENTATION:
                toggleOrientation();
                break;
        }
    }

    private void toggleOrientation() {
        int currentOrientation = getResources().getConfiguration().orientation;

        if (currentOrientation == 2) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    private void togglePageLayout() {
        chatOpen = !chatOpen;

        if (chatOpen) {
            mFragmentVideoHud.setVideoWeight(65);
            mFragmentChat.setChatWeight(35);
        } else {
            mFragmentVideoHud.setVideoWeight(0);
            mFragmentChat.setChatWeight(100);
        }
    }

    private static Integer count2 = 0;

    private Emitter.Listener socketOnConnect = new Emitter.Listener() {

        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    requestNextUser();
                }
            });
        }
    };

    private final void closeWebSocket() {
        try {
            mSocket
                    .off("chat/users/next", onNextUserData)
                    .off(Socket.EVENT_CONNECT, this.socketOnConnect);

            SocketIO.destroyConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // onTouchEvent () method gets called when User performs any touch event on screen
    // Method to handle touch event like left to right swap and right to left swap
    float x1, x2, y1, y2;

    public boolean onTouchEvent(MotionEvent touchevent) {
        switch (touchevent.getAction()) {
            // when user first touches the screen we get x and y coordinate
            case MotionEvent.ACTION_DOWN: {
                x1 = touchevent.getX();
                y1 = touchevent.getY();
                break;
            }
            case MotionEvent.ACTION_UP: {
                x2 = touchevent.getX();
                y2 = touchevent.getY();

                // left to right
                if (x1 < x2) {
                    //Toast.makeText(this, "Left to Right Swap Performed", Toast.LENGTH_LONG).show();
                }

                // Right to left
                if (x1 > x2) {
                    //Toast.makeText(this, "Right to Left Swap Performed", Toast.LENGTH_LONG).show();
                    requestNextUser();
                }

                // UP to Down
                if (y1 < y2) {
                    //Toast.makeText(this, "UP to Down Swap Performed", Toast.LENGTH_LONG).show();
                }

                // Down to UP
                if (y1 > y2) {
                    //Toast.makeText(this, "Down to UP Swap Performed", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (mFragmentSlideMenu.isOpen())
            mFragmentSlideMenu.toggle();
        else
            super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        this.closeWebSocket();

        finish();
    }
}