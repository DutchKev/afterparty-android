package com.example.afterparty.app.activities.conversation;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.afterparty.app.R;
import com.example.afterparty.app.db.DBHelper;
import com.example.afterparty.app.service.SocketService;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Ack;
import com.github.nkzawa.socketio.client.Socket;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ConversationPage extends Activity {
    private static final String TAG = "ConversationPage";

    private ListView listView;
    private ConversationAdapter adapter;
    private ArrayList<Map> messages;

    private Socket mSocket;

    private DBHelper DB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(null);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_conversation);

        listView = (ListView) findViewById(R.id.messagesListView);

        // Create arrayList
        messages = new ArrayList<>();

        // Create adapter
        adapter = new ConversationAdapter(this, messages);

        // Assign adapter to ListView
        listView.setAdapter(adapter);

        DB = new DBHelper(this);

        JSONObject opt = new JSONObject();
        try {
            opt.put("to", getID());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket = SocketService.getSocket(this);
        //mSocket.emit("conversation/history", opt);
        //mSocket.on("conversation/message", onMessage);
        //mSocket.once("conversation/history", onHistory);

        getMessages();

        setHeaderName();
        bindInput();
    }

    @Override
    public void onNewIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null){
            if (extras.containsKey("USER_ID"))
            {
                Toast.makeText(getApplicationContext(), "onNewIntent", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void getMessages() {
        ArrayList<Map> messages = DB.getMessages(getID());

        //addMessages(messages);
    }

    private void addMessages(ArrayList<Map> messagesArray) {

        if (messagesArray.size() == 0) {
            toggleNothingFoundText(View.VISIBLE);
        } else {
            toggleNothingFoundText(View.GONE);

            for (Map m : messagesArray) {
                messages.add(m);
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.notifyDataSetChanged();
                    scrollListViewToBottom();
                }
            });
        }
    }

    private void setHeaderName() {
        String username = getUsername();

        if (username != null) {
            ((TextView)findViewById(R.id.header_text)).setText(username);
        }
    }

    private String getID() {
        Bundle extras = getIntent().getExtras();
        return extras != null ? extras.getString("USER_ID") : null;
    }

    private String getUsername() {
        Bundle extras = getIntent().getExtras();
        return extras != null ? extras.getString("USER_NAME") : null;
    }

    private Emitter.Listener onHistory = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONArray result = ((JSONArray) args[0]);

            //addMessages(result);
        }
    };

    private Emitter.Listener onMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //addMessages((JSONArray) args[0]);
                }
            });
        }
    };

    private void bindInput() {

        final EditText edittext = (EditText) findViewById(R.id.input);

        edittext.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press

                    String msgText = edittext.getText().toString().trim();

                    if (msgText.length() == 0)
                        return false;

                    // Clear text input
                    edittext.setText("");

                    Map message = new HashMap();
                    message.put("conversation_id", getID());
                    message.put("to", getID());
                    message.put("message", msgText);
                    message.put("isSelf", true);

                    DB.insertMessage(getID(), msgText, true);

                    ArrayList messageArr = new ArrayList();
                    JSONObject messageObj = new JSONObject();

                    try {
                        messageObj.put("message", msgText);
                        messageObj.put("to", getID());
                        messageObj.put("isSelf", true);

                        mSocket.emit("conversation/message", message);

                        messageArr.add(message);
                        addMessages(messageArr);

                    } catch (JSONException ignored) {

                    }

                    return false;
                }
                return false;
            }
        });
    }

    private void toggleNothingFoundText(final int state) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                findViewById(R.id.nothingFound).setVisibility(state);
            }
        });
    }

    private void scrollListViewToBottom() {
        listView.post(new Runnable() {
            @Override
            public void run() {
                listView.setSelection(adapter.getCount() - 1);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.i(TAG, "onDestroy");

        mSocket.off("conversation/history");
        mSocket.off("conversation/message");

        finish();
    }
}

