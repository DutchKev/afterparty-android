package com.example.afterparty.app.fragments.chat;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.example.afterparty.app.R;
import com.example.afterparty.app.util.SocketIO;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FragmentChat extends Fragment {

    private static View mRootView;
    private static ListView listView;

    private static ChatAdapter adapter;
    private static ArrayList<JSONObject> listItems;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_chat, container, false);

        listView = (ListView) mRootView.findViewById(R.id.chatList);

        // Create arrayList
        listItems = new ArrayList<JSONObject>();

        // Create adapter
        adapter = new ChatAdapter(mRootView.getContext(), listItems);

        // Assign adapter to ListView
        listView.setAdapter(adapter);

        SocketIO.getInstance(mRootView.getContext())
                .on("message", onNewMessages)
                .emit("chat/history");

        bindInput();

        return mRootView;
    }

    private Emitter.Listener onNewMessages = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        addMessages((JSONArray) args[0]);
                    } catch (JSONException e) {
                        Log.d("IO chat msg error", e.getMessage());
                    }
                }
            });
        }
    };

    private void sendMessage(String message) {
        Socket socket = SocketIO.getInstance(mRootView.getContext());

        socket.emit("chat/message", message);
    }

    public void setChatWeight(int number) {
        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        p.weight = number;
        this.getView().setLayoutParams(p);
    }

    private void addMessages(JSONArray messages) throws JSONException {
        for (int i = 0; i < messages.length(); ++i) {
            listItems.add(messages.getJSONObject(i));
        }

        adapter.notifyDataSetChanged();
        scrollListViewToBottom();
    }

    private void scrollListViewToBottom() {
        listView.post(new Runnable() {
            @Override
            public void run() {
                listView.setSelection(adapter.getCount() - 1);
            }
        });
    }

    private void bindInput() {

        final EditText edittext = (EditText) mRootView.findViewById(R.id.chatEditText);

        edittext.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press

                    String msgText = edittext.getText().toString();

                    // Clear text input
                    edittext.setText("");

                    JSONArray messageArr = new JSONArray();
                    JSONObject messageObj = new JSONObject();

                    try {
                        messageObj.put("username", "me");
                        messageObj.put("message", msgText);

                        messageArr.put(messageObj);

                        addMessages(messageArr);

                        sendMessage(msgText);
                    } catch (JSONException ignored) {

                    }

                    return false;
                }
                return false;
            }
        });
    }

    @Override
    public final void onDestroy() {
        SocketIO.getInstance(mRootView.getContext()).off("message", this.onNewMessages);
        super.onDestroy();
    }
}