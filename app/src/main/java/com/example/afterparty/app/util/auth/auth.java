package com.example.afterparty.app.util.auth;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.Toast;
import com.example.afterparty.app.Constants;
import com.example.afterparty.app.Globals;
import com.example.afterparty.app.activities.HomePage;
import com.example.afterparty.app.activities.LoginPage;
import com.example.afterparty.app.activities.MainActivity;
import com.example.afterparty.app.service.SocketService;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.http.Header;
import org.json.JSONObject;

public class Auth {

    private final static String TAG = "AUTH";

    public static void checkToken(final Context context, final Runnable onSuccess, final Runnable onFail) {
        RequestParams params = new RequestParams();
        params.put("token", Globals.getLocalToken(context));

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(Constants.getNodeUrl() + "/auth", params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                onSuccess.run();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                Log.i(TAG, "Token error: " + Integer.toString(statusCode));

                switch (statusCode) {
                    case 401:
                        onFail.run();
                        break;
                    case 0:
                    case 500:
                    default:
                        Toast.makeText(context, "Server is unavailable", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }

    private void checkVersion(Context context, final Runnable onSuccess, final Runnable onFail) {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            int versionCode = pInfo.versionCode;
            String versionName = pInfo.versionName;

            new AsyncHttpClient().get(Constants.getNodeUrl() + "/android/info", null, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String text, Throwable throwable) {

                }
            });

            Toast.makeText(context, Integer.toString(versionCode), Toast.LENGTH_SHORT).show();
            Toast.makeText(context, versionName, Toast.LENGTH_SHORT).show();

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}
