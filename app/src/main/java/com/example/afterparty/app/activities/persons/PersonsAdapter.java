package com.example.afterparty.app.activities.persons;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.afterparty.app.Constants;
import com.example.afterparty.app.R;
import org.json.JSONObject;

import java.util.List;

class PersonsViewHolder{
    public TextView message;
    public TextView username;
    public TextView country;
    public TextView genderPlusAge;
    public ImageView profileImage;
}

public class PersonsAdapter extends BaseAdapter {
    private Context context;

    private List<JSONObject> persons;

    public PersonsAdapter(Context context, List<JSONObject> listPersons) {
        this.context = context;
        this.persons = listPersons;
    }

    public int getCount() {
        return persons.size();
    }

    public Object getItem(int position) {
        return persons.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup viewGroup) {

        PersonsViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.person_row, null);

            holder = new PersonsViewHolder();
            holder.profileImage = (ImageView) convertView.findViewById(R.id.profileImage);
            holder.username = (TextView) convertView.findViewById(R.id.username);
            holder.genderPlusAge = (TextView) convertView.findViewById(R.id.genderPlusAge);
            holder.country = (TextView) convertView.findViewById(R.id.country);
            //holder.message = (TextView) convertView.findViewById(R.id.message);

            convertView.setTag(holder);
        } else {
            holder = (PersonsViewHolder) convertView.getTag();
        }

        JSONObject data = persons.get(position);
        String gender   = data.optString("gender");

        if (gender.equals(Constants.GENDER_MALE)) {
            holder.profileImage.setImageResource(R.drawable.user_male);
            //holder.username.setTextColor(Color.parseColor("#99D3F2"));
        } else {
            holder.profileImage.setImageResource(R.drawable.user_female);
            //holder.username.setTextColor(Color.parseColor("#E999F2"));
        }

        holder.username.setText(data.optString("username"));
        holder.genderPlusAge.setText(data.optString("gender") + ", " + data.optString("age"));
        holder.country.setText(data.optString("country"));

        return convertView;
    }
}
