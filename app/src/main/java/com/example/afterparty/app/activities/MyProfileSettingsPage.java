package com.example.afterparty.app.activities;

import android.app.*;
import android.content.Intent;
import android.os.*;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.view.*;
import android.widget.Button;
import com.example.afterparty.app.Globals;
import com.example.afterparty.app.R;
import com.example.afterparty.app.service.SocketService;

import java.util.List;

public class MyProfileSettingsPage extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.myprofilesettings);

        Preference somePref = getPreferenceScreen().findPreference("logout");

        somePref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                //getPreferenceScreen().removePreference(preference);

                Globals.deleteLocalToken(getApplicationContext());

                stopService(new Intent(MyProfileSettingsPage.this, SocketService.class));

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();

                return true;
            }
        });
    }

}
