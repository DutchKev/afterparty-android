package com.example.afterparty.app.activities.conversation.overview;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import com.example.afterparty.app.R;
import com.example.afterparty.app.activities.conversation.ConversationPage;
import com.example.afterparty.app.activities.profile.ProfilePage;
import com.example.afterparty.app.service.SocketService;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ConversationOverviewPage extends Activity {
    private static final String TAG = "ConversationOverviewPage";

    private ListView listView;
    private ConversationOverviewAdapter adapter;
    private ArrayList<ConversationOverviewMessage> messages;

    Socket mSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_conversation_overview);

        // Create arrayList
        messages = new ArrayList<>();

        // Create adapter
        adapter = new ConversationOverviewAdapter(this, messages);

        listView = (ListView) findViewById(R.id.messagesListView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(onClickItem);

        mSocket = SocketService.socket;
        mSocket.on("conversation/overview", onOverview);
        mSocket.emit("conversation/overview");
    }

    private final AdapterView.OnItemClickListener onClickItem = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent intent = new Intent(getApplicationContext(), ConversationPage.class);
            intent.putExtra("USER_ID", messages.get(position).otherUserID);
            intent.putExtra("USER_NAME", messages.get(position).otherUsername);
            startActivity(intent);
        }
    };

    private Emitter.Listener onOverview = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    addMessages(((JSONArray) args[0]));
                }
            });
        }
    };

    private void addMessages(JSONArray messagesArray) {
        if (messagesArray.length() == 0) {
            findViewById(R.id.nothingFound).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.nothingFound).setVisibility(View.GONE);

            for (int i = 0; i < messagesArray.length(); ++i) {
                JSONObject message = messagesArray.optJSONObject(i);
                messages.add(new ConversationOverviewMessage(message.optJSONObject("message")));
            }

            adapter.notifyDataSetChanged();
            scrollListViewToBottom();
        }

    }

    private void scrollListViewToBottom() {
        listView.post(new Runnable() {
            @Override
            public void run() {
                listView.setSelection(adapter.getCount() - 1);
            }
        });
    }
}

