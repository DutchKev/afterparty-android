package com.example.afterparty.app.activities.conversation;

public class ConversationMessage {
    /**
     * The content of the message
     */
    String message;
    /**
     * boolean to determine, who is sender of this message
     */
    boolean isSelf;
    /**
     * boolean to determine, whether the message is a status message or not.
     * it reflects the changes/updates about the sender is writing, have entered text etc
     */
    boolean isStatusMessage;

    /**
     * Constructor to make a Message object
     */
    public ConversationMessage(String message, boolean isSelf) {
        super();
        this.message = message;
        this.isSelf = isSelf;
        this.isStatusMessage = false;
    }
    /**
     * Constructor to make a status Message object
     * consider the parameters are swaped from default Message constructor,
     *  not a good approach but have to go with it.
     */
    public ConversationMessage(boolean status, String message) {
        super();
        this.message = message;
        this.isSelf = false;
        this.isStatusMessage = status;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public boolean isSelf() {
        return isSelf;
    }
    public void isSelf(boolean isSelf) {
        this.isSelf = isSelf;
    }
    public boolean isStatusMessage() {
        return isStatusMessage;
    }
    public void setStatusMessage(boolean isStatusMessage) {
        this.isStatusMessage = isStatusMessage;
    }


}
