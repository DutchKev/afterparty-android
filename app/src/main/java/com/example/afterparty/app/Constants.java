package com.example.afterparty.app;

public class Constants {

    public static final String MODE_DEV = "DEV";
    public static final String MODE_LIVE = "LIVE";

    public static final String DEV_DEVICE_NAME = "UNKNOWN";
    public static final String DEV_DEVICE_NAME_MOBILE = "Samsung GT-I9195";

    /* Constants local */
    public static final String IP_WOWZA_LOCAL = "192.168.57.1";
    public static final String URL_WOWZA_LOCAL = "rtsp://" + IP_WOWZA_LOCAL;

    public static final String IP_NODE_SERVER_LOCAL = "192.168.178.29:8010";
    public static final String URL_NODE_SERVER_LOCAL = "http://" + IP_NODE_SERVER_LOCAL;

    /* Constants live */
    public static final String IP_NODE_SERVER_LIVE = "149.210.227.14:8010";
    public static final String URL_NODE_SERVER_LIVE = "http://" + IP_NODE_SERVER_LIVE;

    public static final String WOWZA_IP_VPS     = "149.210.227.14";
    public static final String WOWZA_PORT_VPS = "1935";

    public static final String GENDER_MALE = "Male";
    public static final String GENDER_FEMALE = "Female";

    public static final String CURRENT_MODE = MODE_DEV;

    public static String getNodeUrl() {
        if (CURRENT_MODE.equals(MODE_LIVE)) {
            return URL_NODE_SERVER_LIVE;
        } else {
            return URL_NODE_SERVER_LOCAL;
        }
    }

    public static String getWowzaIp() {
        if (CURRENT_MODE.equals(MODE_LIVE)) {
            return WOWZA_IP_VPS;
        } else {
            return IP_WOWZA_LOCAL;
        }
    }

}
