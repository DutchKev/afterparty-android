package com.example.afterparty.app.activities.profile.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.afterparty.app.R;
import com.example.afterparty.app.activities.MyProfileSettingsPage;
import com.example.afterparty.app.activities.conversation.ConversationPage;
import org.json.JSONException;
import org.json.JSONObject;

public class ProfileDataFragment extends Fragment {

    private View mRootView;
    private String id;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mRootView = inflater.inflate(R.layout.fragment_profile_data, container, false);

        mRootView.findViewById(R.id.btn_edit_profile).setOnClickListener(onClickEditProfile);
        mRootView.findViewById(R.id.btn_open_conversation).setOnClickListener(onClickOpenConversation);

        return mRootView;
    }

    public void setId(String _id) {
        id = _id;
    }

    private final View.OnClickListener onClickOpenConversation = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(mRootView.getContext(), ConversationPage.class);
            intent.putExtra("USER_ID", id);
            startActivity(intent);
        }
    };

    private final View.OnClickListener onClickEditProfile = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(mRootView.getContext(), MyProfileSettingsPage.class);
            startActivity(intent);
        }
    };

    public void showEditProfileBtn() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mRootView.findViewById(R.id.btn_edit_profile).setVisibility(View.VISIBLE);
            }
        });
    }

    public void showSendMessageBtn() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mRootView.findViewById(R.id.btn_open_conversation).setVisibility(View.VISIBLE);
            }
        });
    }

    public void insertData(final JSONObject data) {
        try {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    ((TextView) mRootView.findViewById(R.id.username)).setText(data.optString("username"));
                    ((TextView) mRootView.findViewById(R.id.gender)).setText(data.optString("gender"));
                    ((TextView) mRootView.findViewById(R.id.age)).setText(data.optString("age"));
                    ((TextView) mRootView.findViewById(R.id.country)).setText(data.optString("country"));

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
