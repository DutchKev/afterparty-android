package com.example.afterparty.app.activities.profile;

import android.os.*;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.*;
import android.widget.Toast;
import com.example.afterparty.app.Constants;
import com.example.afterparty.app.Globals;
import com.example.afterparty.app.R;
import com.example.afterparty.app.activities.profile.fragments.ProfileDataFragment;
import com.example.afterparty.app.service.SocketService;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Ack;
import com.github.nkzawa.socketio.client.Socket;
import org.json.JSONException;
import org.json.JSONObject;

public class ProfilePage extends FragmentActivity {

    private static final String TAG = "ProfilePage";

    Socket mSocket;
    ProfileDataFragment mProfileDataFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_profile);

        mSocket = SocketService.getSocket(getApplicationContext());

        mProfileDataFragment = (ProfileDataFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_profile_data);
        mProfileDataFragment.setId(getID());

        if (isOwn()) {
            mProfileDataFragment.showEditProfileBtn();
        } else {
            mProfileDataFragment.showSendMessageBtn();
        }

        loadProfileData();
    }

    public String getID() {
        Bundle extras = getIntent().getExtras();
        return extras != null ? extras.getString("USER_ID") : null;
    }

    private Boolean isOwn() {
        Bundle extras = getIntent().getExtras();
        return extras != null && extras.getBoolean("OWN_PROFILE");
    }

    private void loadProfileData() {
        JSONObject params = new JSONObject();

        try {
            params.put("id", this.getID());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mSocket.on("/user", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                mProfileDataFragment.insertData((JSONObject) args[0]);
            }
        });

        mSocket.emit("/user", params);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.i(TAG, "onDestroy");

        mSocket.off("/user");
        mSocket = null;

        finish();
    }
}
