package com.example.afterparty.app;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import com.loopj.android.http.JsonHttpResponseHandler;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class Globals {

    public static String BUILD_DEVICE_NAME = getDeviceName();

    private static String getSharedPreference(Context context, String key) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString(key, ""/*default value*/);
    }

    private static void setSharedPreference(Context context, String key, String value) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getLocalToken(Context context) {
        return getSharedPreference(context, "TOKEN");
    }

    public static void setLocalToken(Context context, String token) {
        setSharedPreference(context, "TOKEN", token);
    }

    public static void deleteLocalToken(Context context) {
        setSharedPreference(context, "TOKEN", "");
    }


    public static String getUserID(Context context) {
        return getSharedPreference(context, "USER_ID");
    }

    public static void setUserID(Context context, String token) {
        setSharedPreference(context, "USER_ID", token);
    }

    public static void deleteUserID(Context context, String token) {
        setSharedPreference(context, "USER_ID", "");
    }

    public static boolean hasToken(Context context) {
        String token = getLocalToken(context);

        if (token == null || token.equals("")) {
            return false;
        } else {
            return true;
        }
    }

    public static final void alert(Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {}
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static final void logError(Context context, String message) {
        try {
            final String error = (message == null) ? "Error unknown" : message;
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(error).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {}
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        } catch (Exception e) {

        }
    }


    public static String getAssetJSON(Context context, String name) {
        String json;

        try {
            InputStream is = context.getAssets().open(name);

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        if (manufacturer.equalsIgnoreCase("HTC")) {
            return "HTC " + model;
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;
        String phrase = "";
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase += Character.toUpperCase(c);
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase += c;
        }
        return phrase;
    }


}