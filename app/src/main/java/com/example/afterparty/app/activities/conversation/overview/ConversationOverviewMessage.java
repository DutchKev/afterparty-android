package com.example.afterparty.app.activities.conversation.overview;

import android.util.Log;
import org.json.JSONObject;

public class ConversationOverviewMessage {
    /**
     * The content of the message
     */
    public String message;
    public String otherUserID;
    public String otherUsername;
    public String otherGender;
    //String otherUserID;

    /**
     * Constructor to make a Message object
     */
    public ConversationOverviewMessage(JSONObject data) {
        super();

        JSONObject otherUser = data.optJSONObject("otherUser");

        this.message = data.optString("message");
        this.otherUserID = otherUser.optString("_id");
        this.otherUsername = otherUser.optString("username");
        this.otherGender = otherUser.optString("gender");
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
