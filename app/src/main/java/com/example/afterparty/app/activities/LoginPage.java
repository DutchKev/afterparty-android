package com.example.afterparty.app.activities;

import android.app.*;
import android.content.*;
import android.os.*;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.example.afterparty.app.Constants;
import com.example.afterparty.app.Globals;
import com.example.afterparty.app.R;
import com.example.afterparty.app.service.SocketService;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.Base64;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginPage extends Activity {

    private final static String TAG = "LoginPage";
    
	@Override
	protected void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.activity_login);

        findViewById(R.id.btnLogin).setOnClickListener(onLoginClickListener);
        findViewById(R.id.btnRegister).setOnClickListener(onRegisterClickListener);
	}

    private final View.OnClickListener onRegisterClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(view.getContext(), RegistrationPage.class);
            startActivity(intent);
        }
    };

    private final View.OnClickListener onLoginClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            String usernameText = ((EditText)findViewById(R.id.username)).getText().toString().trim();
            String passwordText = ((EditText)findViewById(R.id.password)).getText().toString().trim();

            if (usernameText.length() == 0 || passwordText.length() == 0)
                return;

            RequestParams params = new RequestParams();
            params.put("email", usernameText);
            params.put("password", passwordText);

            new AsyncHttpClient().post(Constants.getNodeUrl() + "/auth", params, new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    LoginPage.this.onSuccess(response);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String text, Throwable throwable) {
                    LoginPage.this.onFailure(statusCode);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    LoginPage.this.onFailure(statusCode);
                }
            });
        }
    };

    private void onSuccess(JSONObject response) {
        String token = response.optString("token");

        Globals.setLocalToken(getApplicationContext(), token);
        Globals.setUserID(getApplicationContext(), response.optString("_id"));
        Toast.makeText(this, response.optString("_id"), Toast.LENGTH_LONG).show();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void onFailure(int statusCode) {
        switch (statusCode) {
            case 0:
            case 404:
                Toast.makeText(LoginPage.this, "Server unavailable", Toast.LENGTH_LONG).show();
                break;
            case 401:
                Toast.makeText(getApplicationContext(), "Wrong username or password", Toast.LENGTH_LONG).show();
                break;
            default:
                Log.e(TAG, "HTTP status: " + Integer.toString(statusCode));
                Toast.makeText(LoginPage.this, "Server error, try again later", Toast.LENGTH_LONG).show();
                break;
        }
    }
}