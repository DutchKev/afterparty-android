package com.example.afterparty.app.service;

import android.app.*;
import android.content.ComponentName;
import android.content.Context;
import android.support.v4.app.NotificationCompat;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.example.afterparty.app.Constants;
import com.example.afterparty.app.Globals;
import com.example.afterparty.app.R;
import com.example.afterparty.app.activities.conversation.ConversationPage;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.github.nkzawa.socketio.client.SocketIOException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SocketService extends Service {

    private final static String TAG = "SocketService";

    public static Socket socket;
    static Context mContext;

    @Override
    public IBinder onBind(Intent arg0) {
        return myBinder;
    }

    private final IBinder myBinder = (IBinder) new LocalBinder();

    public class LocalBinder extends Binder {
        public SocketService getService() {
            return SocketService.this;
        }
    }


    @Override
    public void onCreate() {
        super.onCreate();
        createSocket(this);
    }

    private static void createSocket(Context context) {
        mContext = context.getApplicationContext();

        String token =  Globals.getLocalToken(context);

        if (token == null || token.equals("")) {
            Log.i(TAG, "token == null");
            return;
        }

        try {
            String url  = Constants.getNodeUrl();

            IO.Options options = new IO.Options();
            options.query = "token=" + token;
            options.forceNew = true;
            //options.reconnection = true;

            socket = IO.socket(url, options);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        socket
                .on("event", socketOnEvent)
                .on(Socket.EVENT_DISCONNECT, socketOnDisconnect)
                .on(Socket.EVENT_CONNECT_ERROR, socketOnConnectError)
                .on(Socket.EVENT_ERROR, socketOnEventError)
                .on("conversation/message", onNewMessage)
                .on("notification", socketOnNotification);

        Log.i(TAG, "new socket created");
    }

    public static Socket getSocket(Context context) {
        if (socket == null) {
            Context appContext = context.getApplicationContext();


            appContext.stopService(new Intent(appContext, SocketService.class));
            appContext.startService(new Intent(appContext, SocketService.class));
        }

        return socket;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Runnable connect = new connectSocket();
        new Thread(connect).start();

        return START_STICKY;
    }

    /*
    public void onStart(Intent intent, int startId){
        super.onStart(intent, startId);
    }
    */

    class connectSocket implements Runnable {

        @Override
        public void run() {
            socket.connect();
        }

    }

    private static String getCurrentActivity() {
        ActivityManager am = (ActivityManager) mContext.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);

        String className  = taskInfo.get(0).topActivity.getClassName();
        className = className.substring(className.lastIndexOf('.') + 1).trim();

        Log.i(TAG, "Current Activity ::" + className);

        return className;
    }

    private static Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            JSONObject message = (JSONObject) args[0];

            // Broadcas
            Intent intent = new Intent("message");
            intent.putExtra("message", message.toString());
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

            String currentActivity = getCurrentActivity();
            Log.i(TAG, currentActivity);

            switch (currentActivity) {
                case "HomePage":
                case "ConversationPage":
                case "ConversationOverviewPage":
                case "ChatPage":
                    break;
                default:
                    createMessageNotification(message);
                    break;
            }
        }
    };

    private static void createMessageNotification(JSONObject value) {
        String title= "Message";
        String body = value.optString("message");
        String id   = value.optString("from_id");

        Intent intent = new Intent(mContext, ConversationPage.class);
        intent.putExtra("USER_ID", id);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pIntent = PendingIntent.getActivity(mContext,0, intent,PendingIntent.FLAG_UPDATE_CURRENT);

        // Build notification
        Notification n = new NotificationCompat.Builder(mContext)
                .setContentTitle(title)
                .setContentText(body)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentIntent(pIntent)
                .setVibrate(new long[] {100, 600})
                .build();

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(NOTIFICATION_SERVICE);

        // hide the notification after its selected
        n.flags |= Notification.FLAG_AUTO_CANCEL;
        n.defaults |= Notification.DEFAULT_SOUND;
        //n.defaults |= Notification.DEFAULT_VIBRATE;
        n.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(1,n);
    }

    private static boolean isOnForeground() {
        // Check if app is on foreground (open)
        ActivityManager activityManager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> services = activityManager.getRunningTasks(Integer.MAX_VALUE);

        return !services.get(0).topActivity.getPackageName().equalsIgnoreCase(mContext.getPackageName());
    }

    private static Emitter.Listener socketOnNotification = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {

            //if (isOnForeground())
                //return;

            JSONObject data = (JSONObject) args[0];
            JSONObject value = data.optJSONObject("value");

            String type = data.optString("type");

            switch (type) {
                case "message":

                    //onNewMessage(value);



                    break;
            }
        }
    };

    private static Emitter.Listener socketOnEvent = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            for (Object o : args) {
                Log.d(TAG, "object: " + o.toString());
                if (o instanceof SocketIOException)
                    ((SocketIOException) o).printStackTrace();
            }
        }
    };

    private static Emitter.Listener socketOnDisconnect = new Emitter.Listener() {

        @Override
        public void call(Object... args) {
            Log.d("Error", "ERROR");
        }
    };

    private static Emitter.Listener socketOnConnectError = new Emitter.Listener() {

        @Override
        public void call(Object... args) {
            Log.d("Error", "eventConnectError");
            for (Object o : args) {
                Log.d(TAG, "object: " + o.toString());
                if (o instanceof SocketIOException)
                    ((SocketIOException) o).printStackTrace();
            }
        }
    };

    private static Emitter.Listener socketOnEventError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d(TAG, "eventError");
        }
    };

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy");

        super.onDestroy();

        if (socket == null) {
            Log.i(TAG, "socket is null");
            return;
        }

        socket.close();

        socket
                .off("event", socketOnEvent)
                .off(Socket.EVENT_DISCONNECT, socketOnDisconnect)
                .off(Socket.EVENT_CONNECT_ERROR, socketOnConnectError)
                .off(Socket.EVENT_ERROR, socketOnEventError);

        socket = null;
    }
}
