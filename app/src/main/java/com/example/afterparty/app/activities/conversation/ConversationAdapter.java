package com.example.afterparty.app.activities.conversation;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.afterparty.app.R;

import java.util.List;
import java.util.Map;

class ViewHolder{
    public TextView message;
}

public class ConversationAdapter extends BaseAdapter {
    private Context context;

    private List<Map> messages;

    public ConversationAdapter(Context context, List<Map> messageList) {
        super();
        this.context = context;
        this.messages = messageList;
    }

    public int getCount() {
        return messages.size();
    }

    public Object getItem(int position) {
        return messages.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        ViewHolder holder;

        Map message = messages.get(position);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.row_conversation, null);

            // Disable click event.
            convertView.setOnClickListener(null);

            holder = new ViewHolder();
            holder.message = (TextView) convertView.findViewById(R.id.message_text);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) holder.message.getLayoutParams();


        if (12 > 24) {
            holder.message.setBackgroundResource(R.drawable.speech_bubble_green);
            lp.gravity = Gravity.RIGHT;
        }
        //If not mine then it is from sender to show orange background and align to left
        else
        {
            holder.message.setBackgroundResource(R.drawable.speech_bubble_orange);
            lp.gravity = Gravity.LEFT;
        }
        holder.message.setLayoutParams(lp);
        holder.message.setTextColor(R.color.textColor);

        holder.message.setText((String)message.get("message"));

        return convertView;
    }
}

