package com.example.afterparty.app.fragments;

import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.example.afterparty.app.Constants;
//import com.example.afterparty.app.Globals;
import com.example.afterparty.app.R;
import com.example.afterparty.app.activities.ChatPage;
import com.example.afterparty.app.camera.CameraPreview;

import java.io.IOException;

public class FragmentVideoHud extends Fragment implements SurfaceHolder.Callback, MediaPlayer.OnPreparedListener {

    public static final String TAG = "FragmentVideoHud";

    public static final int CLICKED_SLIDE_MENU = 0;
    public static final int CLICKED_NEXT_CHAT = 1;
    public static final int CLICKED_TOGGLE_CHAT = 2;
    public static final int CLICKED_TOGGLE_SOUND = 3;
    public static final int CLICKED_TOGGLE_ORIENTATION = 4;

    private View mRootView;
    private TextView mHeaderTextView;
    private VideoView mVideoView;
    private SurfaceView mSurfaceView;
    private SurfaceHolder mSurfacHolder;
    private RelativeLayout mCameraContainer;
    public CameraPreview mCameraPreview;

    private MediaPlayer mMediaPlayer;
    private AudioManager mAudioManager;

    private boolean soundActive = true; // TODO get saved state

    public String currentVideoUrl;
    private Boolean isPlaying = false;

    private int count;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_video_hud, container, false);

        mHeaderTextView = (TextView) mRootView.findViewById(R.id.headerName);

        mSurfaceView = (SurfaceView)mRootView.findViewById(R.id.video_view2);

        mSurfacHolder = mSurfaceView.getHolder();
        mSurfacHolder.addCallback(this);

        mRootView.findViewById(R.id.btn_nextChat).setOnClickListener(onClickListener);
        mRootView.findViewById(R.id.btn_persons).setOnClickListener(onClickListener);
        mRootView.findViewById(R.id.btn_slide_memu).setOnClickListener(onClickListener);
        mRootView.findViewById(R.id.btnChatToggle).setOnClickListener(onClickListener);
        mRootView.findViewById(R.id.btnVideoSound).setOnClickListener(onClickListener);
        mRootView.findViewById(R.id.btnVideoSize).setOnClickListener(onClickListener);

        //mVideoView = (VideoView) mRootView.findViewById(R.id.video_view);

        //mVideoView.setOnErrorListener(onErrorListener);

        mAudioManager = (AudioManager) getActivity().getSystemService(mRootView.getContext().AUDIO_SERVICE);

        mCameraContainer = (RelativeLayout) mRootView.findViewById(R.id.camera_preview_container);
        mCameraPreview = new CameraPreview(mRootView.getContext(), mCameraContainer);

        return mRootView;
    }

    public void createVideoPlayer() {
        try {
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setDisplay(mSurfacHolder);
            //mMediaPlayer.setDataSource(currentVideoUrl);
            //mMediaPlayer.prepare();
            mMediaPlayer.setOnPreparedListener(this);
            mMediaPlayer.setOnErrorListener(onErrorListener);
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        } catch (IllegalArgumentException | IllegalStateException | SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void releaseVideoPlayer() {
        mMediaPlayer.release();
    }

    public View.OnClickListener onClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_slide_memu:
                    ((ChatPage) getActivity()).onHudButtonClick(CLICKED_SLIDE_MENU);
                    break;
                case R.id.btn_nextChat:
                    ((ChatPage) getActivity()).onHudButtonClick(CLICKED_NEXT_CHAT);
                    break;
                case R.id.btn_persons:
                    togglePersonsOverview();
                    break;
                case R.id.btnChatToggle:
                    ((ChatPage) getActivity()).onHudButtonClick(CLICKED_TOGGLE_CHAT);
                    break;
                case R.id.btnVideoSound:
                    ((ChatPage) getActivity()).onHudButtonClick(CLICKED_TOGGLE_SOUND);
                    toggleSound(view);
                    break;
                case R.id.btnVideoSize:
                    ((ChatPage) getActivity()).onHudButtonClick(CLICKED_TOGGLE_ORIENTATION);
                    break;
            }
        }
    };

    public void togglePersonsOverview() {

    }

    public void updateHeaderName(String name, String gender) {
        mHeaderTextView.setText(name);

        if (gender.equals(Constants.GENDER_MALE)) {
            mHeaderTextView.setTextColor(Color.parseColor("#99D3F2"));
        } else {
            mHeaderTextView.setTextColor(Color.parseColor("#E999F2"));
        }
    }

    public void setVideoPlayback(String streamName) {
        if (mMediaPlayer == null)
            return;

        if (streamName == null)
            streamName = currentVideoUrl;
        else
            currentVideoUrl = streamName;

        Log.i(TAG, streamName);

        mMediaPlayer.reset();

        isPlaying = false;

        String WowzaIp = Constants.getWowzaIp();
        try {
            mMediaPlayer.setDataSource("rtsp://" + WowzaIp + ":1935/" + streamName);
            //mMediaPlayer.setDataSource("rtsp://" + WowzaIp + ":1935/vod/sample.mp4");
            mMediaPlayer.prepareAsync();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isPlaying() {
        Boolean result;
        try {
            result = mVideoView != null && mVideoView.isPlaying();
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    public void startVideo() {
        stopVideo();
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    mVideoView.requestFocus();
                    mVideoView.start();
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                }
            }
        });
    }

    public void pauseVideo() {
        if (isPlaying())
            try {
                mVideoView.pause();
            } catch(Exception e) {
                Log.d("Error", e.getMessage());
            }
    }

    public void stopVideo() {
        if (isPlaying())
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        mVideoView.stopPlayback();
                    } catch (Exception e) {
                        Log.d("Error", e.getMessage());
                    }
                }
            });

    }

    public void toggleSound(View view) {
        soundActive = !soundActive;
        view.setBackgroundResource(soundActive ? R.drawable.sound_on : R.drawable.sound_off);

        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, soundActive ? 100 : 0, 0);
    }

    public final void setVideoWeight(int number) {
        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        p.weight = number;
        mRootView.setLayoutParams(p);
    }

    private MediaPlayer.OnErrorListener onErrorListener = new MediaPlayer.OnErrorListener(){
        @Override
        public boolean onError(MediaPlayer mp, int what, int extra){
            Log.e(TAG, "Error! Biatch: " + what);

            switch(what) {
                case MediaPlayer.MEDIA_ERROR_IO:
                    break;
                case MediaPlayer.MEDIA_ERROR_TIMED_OUT:
                    break;
                case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                //case MediaPlayer.MEDIA_ERROR_UNKNOWN:
                default:
                    //mMediaPlayer.reset();
                    mCameraPreview.restartStream();
                    //setVideoPlayback(null);
                    break;
            }

            return true;
        }
    };

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.i(TAG, "onSurfaceCreated");

        createVideoPlayer();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.i(TAG, "onSurfaceChanged");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.i(TAG, "onSurfaceDestroyed");
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        Log.i(TAG, "onPrepared");
        mp.start();

        isPlaying = true;
    }

    @Override
    public final void onDestroy() {
        Log.i(TAG, "onDestroy");

        super.onDestroy();

        mCameraPreview.onDestroy();
        mMediaPlayer.stop();
        //stopVideo();
    }
}
