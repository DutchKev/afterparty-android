package com.example.afterparty.app.activities;

import android.app.*;
import android.content.*;
import android.os.*;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.example.afterparty.app.R;
import com.example.afterparty.app.activities.conversation.overview.ConversationOverviewPage;
import com.example.afterparty.app.activities.persons.PersonsPage;
import com.example.afterparty.app.activities.profile.ProfilePage;
import com.example.afterparty.app.service.SocketService;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HomePage extends Activity {

    private static final String TAG = "HomePage";

    private Boolean backPressed;

    private Socket mSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_home);

        mSocket = SocketService.getSocket(this);
        mSocket.emit("user/activities");
        mSocket.on("user/activities", onUserActivities);

        // CamRoulette button
        findViewById(R.id.camRoulette).setOnClickListener(onClickCamRoulette);
        findViewById(R.id.myProfile).setOnClickListener(onClickMyProfile);
        findViewById(R.id.people).setOnClickListener(onClickPersons);
        findViewById(R.id.messages).setOnClickListener(onClickMessages);

        // Register mMessageReceiver to receive messages.
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("message"));

        backPressed = false;
    }

    private Emitter.Listener onUserActivities = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.i(TAG, "UPDATE!");
            updateUserMessageNotification((JSONObject) args[0]);

            //Toast.makeText(HomePage.this, result.optString("unread"), Toast.LENGTH_LONG).show();
        }
    };

    public void updateUserMessageNotification(JSONObject activities) {
        Log.i(TAG, activities.toString());

        int unread = Integer.parseInt(activities.optString("unread"));

        if (unread > 0) {
            showNotificationCircle("message", activities.optString("unread"));
        } else {
            hideNotificationCircle("message");
        }
    }

    // handler for received Intents for the "my-event" event
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            try {
                String jsonString   = intent.getStringExtra("message");
                JSONObject value    = new JSONObject(jsonString);

                Log.i(TAG, "Got message: " + jsonString);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private void showNotificationCircle(String type, final String value) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView mCircle = (TextView) findViewById(R.id.messagesCircle);
                mCircle.setText(value);
                mCircle.setVisibility(TextView.VISIBLE);
            }
        });
    }

    private void hideNotificationCircle(String type) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView mCircle = (TextView) findViewById(R.id.messagesCircle);
                mCircle.setVisibility(TextView.GONE);
            }
        });
    }

    private final View.OnClickListener onClickCamRoulette = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(getApplicationContext(), MainMenuPage.class);
            startActivity(intent);
        }
    };

    private final View.OnClickListener onClickMessages = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(getApplicationContext(), ConversationOverviewPage.class);
            startActivity(intent);
        }
    };

    private final View.OnClickListener onClickMyProfile = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(getApplicationContext(), ProfilePage.class);
            intent.putExtra("OWN_PROFILE", true);
            startActivity(intent);
        }
    };

    private final View.OnClickListener onClickPersons = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(getApplicationContext(), PersonsPage.class);
            startActivity(intent);
        }
    };

    @Override
    public void onBackPressed() {
        if (!backPressed) {
            backPressed = true;
            Toast.makeText(this, "Press again to leave", Toast.LENGTH_SHORT).show();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

        Log.i(TAG, "onResume");

        mSocket.emit("user/activities", onUserActivities);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mSocket.off("user/activities", onUserActivities);
        mSocket = null;
    }
}
