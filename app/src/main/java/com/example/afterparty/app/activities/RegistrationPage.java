package com.example.afterparty.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.*;
import com.example.afterparty.app.Constants;
import com.example.afterparty.app.R;
import com.example.afterparty.app.util.location.MyLocation;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class RegistrationPage extends Activity implements View.OnClickListener {

    String countryText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_registration);

        countryText = this.getResources().getConfiguration().locale.getDisplayCountry();

        // Bind login button
        Button button_Register = (Button) findViewById(R.id.btnRegister);
        button_Register.setOnClickListener(this);

        MyLocation.LocationResult locationResult = new MyLocation.LocationResult(){
            @Override
            public void gotLocation(Location location){
                //Got the location!

                if (location != null) {
                    Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

                    List<Address> addresses;
                    try {
                        addresses = geocoder.getFromLocation(location.getLatitude(),location.getLongitude(), 1);
                        countryText = addresses.get(0).getCountryName();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        MyLocation myLocation = new MyLocation();
        myLocation.getLocation(this, locationResult);
    }

    private void setCountryPicker() {
        /*
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        CountryPicker picker = new CountryPicker();
        picker.setListener(new CountryPickerListener() {

            @Override
            public void onSelectCountry(String name, String code) {
                Toast.makeText(
                        MainActivity.this,
                        "Country Name: " + name + " - Code: " + code
                                + " - Currency: "
                                + CountryPicker.getCurrencyCode(code),
                        Toast.LENGTH_SHORT).show();
            }
        });

        transaction.replace(R.id.home, picker);

        transaction.commit();
*/
    }

    @Override
    public void onClick(View v) {
        EditText username       = (EditText)findViewById(R.id.username);
        EditText email          = (EditText)findViewById(R.id.email);
        EditText password       = (EditText)findViewById(R.id.password);
        EditText passwordRepeat = (EditText)findViewById(R.id.passwordRepeat);
        Spinner gender          = (Spinner) findViewById(R.id.gender);

        DatePicker datePicker   = (DatePicker) findViewById(R.id.dateOfBirth);

        String emailText    = email.getText().toString().trim();
        String usernameText = username.getText().toString().trim();
        String passwordText = password.getText().toString().trim();
        String passwordRepeatText = passwordRepeat.getText().toString().trim();
        String genderText   = gender.getSelectedItem().toString().trim();
        // Date of birth
        long dateTime = datePicker.getCalendarView().getDate();
        Date date = new Date(dateTime);
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        String dateOfBirthText = dateFormat.format(date);

        if (emailText.equals("") || usernameText.equals("") || passwordText.equals("") || passwordRepeatText.equals("")) {
            Toast.makeText(this, "Not all details are filled in", Toast.LENGTH_LONG).show();
        //} else if (!password.equals(passwordRepeatText)){
          //  Toast.makeText(this, "Passwords do not match", Toast.LENGTH_LONG).show();
        } else {

            RequestParams params = new RequestParams();
            params.put("username", usernameText);
            params.put("password", passwordText);
            params.put("email", emailText);
            params.put("gender", genderText);
            params.put("date_of_birth", dateOfBirthText);
            params.put("country", countryText);

            AsyncHttpClient client = new AsyncHttpClient();
            client.post(Constants.getNodeUrl() + "/register", params, new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    JSONObject err = response.optJSONObject("error");
                    if (err != null) {
                        showError(err);
                    } else {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            });
        }
    }

    private final int genderStringToConstant(String gender) {
        int constant = 0;
        if (gender.equals("Male")) {
            constant = 1;
        }
        else if (gender.equals("Female")) {
            constant = 2;
        }
        else if (gender.equals("Other")) {
            constant = 3;
        }

        return constant;
    }

    /**
     * handle the error
     *
     * @param object
     */
    private final void showError(JSONObject object) {
        JSONObject errors = object.optJSONObject("errors");
        String message = null;

        if (errors.optJSONObject("username") != null) {
            message = errors.optJSONObject("username").optString("message");
        }
        else if (errors.optJSONObject("email") != null) {
             message = errors.optJSONObject("email").optString("message");
        }
        else if (errors.optJSONObject("password") != null) {
            message = errors.optJSONObject("password").optString("message");
        }

        if (message != null) {
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }
    }
}
