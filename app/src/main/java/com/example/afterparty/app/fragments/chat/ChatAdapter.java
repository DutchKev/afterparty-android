package com.example.afterparty.app.fragments.chat;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.afterparty.app.Constants;
import com.example.afterparty.app.R;
import org.json.JSONObject;

import java.util.List;

class ViewHolder{
    public TextView message;
    public TextView username;
}

public class ChatAdapter extends BaseAdapter {
    private Context context;

    private List<JSONObject> chat;

    public ChatAdapter(Context context, List<JSONObject> listChat) {
        this.context = context;
        this.chat = listChat;
    }

    public int getCount() {
        return chat.size();
    }

    public Object getItem(int position) {
        return chat.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup viewGroup) {

        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.chat_row, null);

            holder = new ViewHolder();
            holder.username = (TextView) convertView.findViewById(R.id.username);
            holder.message = (TextView) convertView.findViewById(R.id.message);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        JSONObject data = chat.get(position);
        String gender   = data.optString("gender");

        if (gender.equals(Constants.GENDER_MALE)) {
            holder.username.setTextColor(Color.parseColor("#99D3F2"));
        } else {
            holder.username.setTextColor(Color.parseColor("#E999F2"));
        }

        holder.username.setText(data.optString("username"));
        holder.message.setText(data.optString("message"));

        return convertView;
    }
}
