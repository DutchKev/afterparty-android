package com.example.afterparty.app.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.example.afterparty.app.Globals;

public class DBHelper extends SQLiteOpenHelper {

    private static final String TAG = "DBHelper";

    public static final String DATABASE_NAME = "DB.db";
    public static final String MESSAGES_TABLE_NAME = "messages";
    public static final String MESSAGES_COLUMN_ID = "id";
    public static final String MESSAGES_COLUMN_MESSAGE = "message";
    public static final String MESSAGES_COLUMN_TIME = "time";
    public static final String MESSAGES_COLUMN_STREET = "street";
    public static final String MESSAGES_COLUMN_CITY = "place";
    public static final String MESSAGES_COLUMN_PHONE = "phone";

    private String MY_DATABASE_NAME;

    public DBHelper(Context context) {
        super(context, Globals.getUserID(context) + "-" + DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS messages " +
                        "(id INTEGER PRIMARY KEY, conversation_id TEXT, message TEXT, is_self boolean, time TEXT)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(TAG, "onUpgrade");

        // TODO Auto-generated method stub
        //db.execSQL("DROP TABLE IF EXISTS messages");
        //onCreate(db);
    }

    public ArrayList<Map> getMessages(String conversationId) {
        ArrayList<Map> returnArray = new ArrayList<>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor result =  db.rawQuery("SELECT * FROM messages", null);
        result.moveToFirst();

        while(!result.isAfterLast()) {
            Map<String,String> temp = new HashMap<>();

            temp.put("message", result.getString(result.getColumnIndex(MESSAGES_COLUMN_MESSAGE)));
            temp.put("time", result.getString(result.getColumnIndex(MESSAGES_COLUMN_TIME)));
            returnArray.add(temp);
        }
        return returnArray;
    }

    public boolean insertMessage(String conversation_id, String message, Boolean isSelf) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("conversation_id", conversation_id);
        contentValues.put("message", message);
        contentValues.put("is_self", isSelf);
        db.insert("messages", null, contentValues);
        return true;
    }

    public boolean updateMessage(Integer id, String name, String phone, String email, String street,String place) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("phone", phone);
        contentValues.put("email", email);
        contentValues.put("street", street);
        contentValues.put("place", place);
        db.update("contacts", contentValues, "id = ? ", new String[]{Integer.toString(id)});
        return true;
    }

    public Cursor getData(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from messages where conversation_id="+id+"", null);
        return res;
    }

    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, MESSAGES_TABLE_NAME);
        return numRows;
    }
}
