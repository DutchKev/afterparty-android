package com.example.afterparty.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.example.afterparty.app.service.SocketService;

public class ServiceBootstrap extends BroadcastReceiver {

    private static final String TAG = "ServiceBootstrap";

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.i(TAG, "Starting SocketIO");

        Intent startServiceIntent = new Intent(context, SocketService.class);
        context.startService(startServiceIntent);
    }
}
