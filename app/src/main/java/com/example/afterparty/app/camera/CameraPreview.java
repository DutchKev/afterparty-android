package com.example.afterparty.app.camera;

import android.content.Context;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.ViewManager;
import android.widget.RelativeLayout;
import com.example.afterparty.app.Constants;
import com.example.afterparty.app.Globals;
import com.example.afterparty.app.util.SocketIO;
import com.github.nkzawa.emitter.Emitter;
import net.majorkernelpanic.streaming.MediaStream;
import net.majorkernelpanic.streaming.Session;
import net.majorkernelpanic.streaming.SessionBuilder;
import net.majorkernelpanic.streaming.audio.AudioQuality;
import net.majorkernelpanic.streaming.rtsp.RtspClient;
import net.majorkernelpanic.streaming.video.VideoQuality;

public class CameraPreview implements SurfaceHolder.Callback, RtspClient.Callback, Session.Callback {
    private final String TAG = this.getClass().getSimpleName();

    int[] list;

    public Session mSession;
    public RtspClient mClient;

    public net.majorkernelpanic.streaming.gl.SurfaceView mSurfaceView;
    public SurfaceHolder mSurfaceHolder;

    public Boolean mSurfaceReady = false;

    private RelativeLayout mCameraContainer;

    private Context mContext;

    private int count = 0;

    public CameraPreview(Context context, RelativeLayout cameraContainer) {

        mContext = context;

        mCameraContainer = cameraContainer;

        setupSurfaceView();

        /*
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        Log.i(TAG, "Timeout");
                        restartStream();
                    }
                },
                30000);
                */
    }

    public void setupSurfaceView() {
        if (mSurfaceView != null)
            destroySurfaceView();

        mSurfaceView = new net.majorkernelpanic.streaming.gl.SurfaceView(mContext);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(160, 120);
        params.setMargins(10, 70, 0, 0);

        mSurfaceView.setLayoutParams(params);
        mSurfaceView.setZOrderOnTop(true);

        mSurfaceHolder = mSurfaceView.getHolder();

        mSurfaceHolder.addCallback(this);

        mCameraContainer.addView(mSurfaceView);
    }

    public void destroySurfaceView() {
        if (mSurfaceView == null)
            return;

        mSurfaceReady = false;

        //mSurfaceView.removeMediaCodecSurface();
        //mSurfaceView.getSurfaceTexture().release();

        mSurfaceHolder.getSurface().release();
        mSurfaceHolder.removeCallback(this);

        ((ViewManager)mSurfaceView.getParent()).removeView(mSurfaceView);

        mSurfaceView    = null;
        mSurfaceHolder  = null;
    }

    public void setupCamera() {

        // Configures the SessionBuilder
        mSession = SessionBuilder.getInstance()
                .setContext(mContext.getApplicationContext())
                .setCallback(this)
                .setSurfaceView(mSurfaceView)
                .setPreviewOrientation(90)
                .setAudioEncoder(SessionBuilder.AUDIO_AAC)
                .setAudioQuality(new AudioQuality(16000, 32000))
                .setVideoEncoder(SessionBuilder.VIDEO_H264)
                .setVideoQuality(new VideoQuality(320, 240, 15, 500000))
                .setTimeToLive(32)
                .build();

        // Use this to force streaming with the MediaRecorder API
        mSession.getVideoTrack().setStreamingMethod(MediaStream.MODE_MEDIARECORDER_API);

        mClient = new RtspClient();
        mClient.setSession(mSession);
        mClient.setCallback(this);

        requestStreamToken();
    }

    public void destroyStream() {
        try {
            mSession.syncStop();
            mSession.release();
            mClient.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void requestStreamToken() {
        Log.i(TAG, "Request Token");
        (SocketIO.getInstance(mContext)).once("user/request/stream-token", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                startStream((String) args[0]);
            }
        }).emit("user/request/stream-token");
    }

    public void startStream(final String streamToken) {
        Log.i(TAG, "Start Stream");

        String ip = Constants.getWowzaIp();
        String port = Constants.WOWZA_PORT_VPS;
        String path = streamToken;

        Log.i(TAG, path);

        mClient.setCredentials("afterparty", "test1234");
        mClient.setServerAddress(ip, Integer.parseInt(port));
        mClient.setStreamPath("/" + path);

        try {
            mClient.startStream();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public void restartStream() {
        Log.i(TAG, "Restarting Camera!");

        new android.os.Handler().postDelayed(new Runnable() {
            public void run() {

                //if (mSession != null && mSession.getVideoTrack() != null) {
                    //mSession.getVideoTrack().
                    //mSession.getVideoTrack().mMediaRecorder.reset();
                //} else {
                    destroyStream();
                destroySurfaceView();
                setupSurfaceView();
                //}
            }
        }, 1000);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.i(TAG, "Surface Created");

        mSurfaceReady = true;

        setupCamera();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.i(TAG, "SurfaceChanged");

        //this.restartStream();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.i(TAG, "Surface Destroyed");
        //mClient.stopStream();

        //this.getHolder().removeCallback(this);

        /*
        boolean retry = true;
        _thread.setRunning(false);
        while (retry) {
            try {
                _thread.join();
                retry = false;
            } catch (InterruptedException e) {
                // we will try it again and again...
            }
        }
        */
    }

    @Override
    public void onSessionStarted() {
        Log.i(TAG, "Session Started");
    }

    @Override
    public void onSessionStopped() {
        Log.i(TAG, "Session Stopped");

        //this.restartStream();
    }

    @Override
    public void onBitrateUpdate(long bitrate) {

    }

    @Override
    public void onSessionError(int message, int streamType, Exception e) {
        if (e != null) {
            Log.e("onSessionError", Integer.toString(message) + ":" + e.getMessage());
            e.printStackTrace();
        } else {
            Log.i(TAG, "Session Error!");
        }

        switch (message) {
            // Camera unavailable
            // Should be released somehow.
            case Session.ERROR_CAMERA_ALREADY_IN_USE:
                restartStream();
                break;

            // SurfaceView is invalid.
            // Recreate.
            case Session.ERROR_INVALID_SURFACE:
                //this.restartStream();
                break;
            default:
                break;
        }
    }

    @Override
    public void onPreviewStarted() {
        Log.i(TAG, "onPreviewStarted");
    }

    @Override
    public void onSessionConfigured() {
        Log.i(TAG, "onSessionConfigured");

        mSurfaceView.setAspectRatioMode(net.majorkernelpanic.streaming.gl.SurfaceView.ASPECT_RATIO_PREVIEW);
    }

    @Override
    public void onRtspUpdate(int message, Exception e) {
        Log.i(TAG, "onRtspUpdate");

        switch (message) {
            case RtspClient.ERROR_CONNECTION_FAILED:
            case RtspClient.ERROR_WRONG_CREDENTIALS:
                Globals.logError(mContext, e.getMessage());
                e.printStackTrace();
                break;
            default:
                Log.e("ABCDEFG", "onRtsUpdate: " + Integer.toString(message));
        }
    }

    public void onDestroy() {
        Log.i(TAG, "onDestroy");

        destroyStream();
        destroySurfaceView();

        mContext = null;
        mSession = null;
        mClient  = null;
        mSurfaceView = null;
        mCameraContainer = null;
        mSurfaceHolder = null;
    }
}