package com.example.afterparty.app.activities.conversation.overview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.afterparty.app.Constants;
import com.example.afterparty.app.R;

import java.util.List;

class ConversationOverviewHolder{
    public TextView message;
    public TextView otherUserName;
    public ImageView profileImage;
}

public class ConversationOverviewAdapter extends BaseAdapter {
    private Context context;

    private List<ConversationOverviewMessage> messages;

    public ConversationOverviewAdapter(Context context, List<ConversationOverviewMessage> messageList) {
        super();
        this.context = context;
        this.messages = messageList;
    }

    public int getCount() {
        return messages.size();
    }

    public Object getItem(int position) {
        return messages.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        ConversationOverviewHolder holder;

        ConversationOverviewMessage message = messages.get(position);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.row_conversation_overview, null);

            holder = new ConversationOverviewHolder();
            holder.message = (TextView) convertView.findViewById(R.id.message_text);
            holder.profileImage = (ImageView) convertView.findViewById(R.id.profileImage);
            holder.otherUserName = (TextView) convertView.findViewById(R.id.otherUserName);

            convertView.setTag(holder);
        } else {
            holder = (ConversationOverviewHolder) convertView.getTag();
        }

        String gender = message.otherGender;

        if (gender.equals(Constants.GENDER_MALE)) {
            holder.profileImage.setImageResource(R.drawable.user_male);
        } else {
            holder.profileImage.setImageResource(R.drawable.user_female);
        }

        holder.otherUserName.setText(message.otherUsername);
        holder.message.setText(message.getMessage());

        return convertView;
    }
}

